#include <stdio.h>

int m = 2;
int n = 2;

int setMatrix(int [m][n]);
void add(int [m][n], int [m][n]);
void mul(int [m][n], int [m][n]);
void display(int [m][n]);


int main(){
	
	int m1[m][n];
	int m2[m][n];

	setMatrix(m1);
	setMatrix(m2);

	display(m1);
	display(m2);

	printf("Addition of matrixes: \n");
	add(m1,m2);

	printf("Multification of matrixes: \n m1*m2\n");
	mul(m1, m2);

	return 0;
}

int setMatrix(int mat[m][n]){
        printf("Enter matrix.\n ");
        for(int i = 0; i < m; i++){
                printf("Row %d\n", i+1);
                for(int j = 0; j < n; j++){
                        printf("Enter no%d: ", j+1);
                        scanf("%d",& mat[i][j]);
                }
        }

	return mat;
}

void add(int m1[m][n], int m2[m][n]){

	int add[m][n];

	for(int i = 0; i < m; i++){
		for(int j = 0; j < n; j++){
			add[i][j] = m1[i][j] + m2[i][j];
		}
	}

	display(add);
}

void mul(int m1[m][n],int m2[m][n]){
	
	int sum = 0;
	int mul[m][n];

	for(int i = 0; i < m; i++){
		for(int j = 0; j < n; j++){
			for(int k = 0; k < n; k++){
				sum += m1[i][k] * m2[k][j];
			}

			mul[i][j] = sum;
			sum = 0;
		}
	}

	display(mul);
}

void display(int mat[m][n]){
        //display answer
        for(int i = 0; i < m;i++){
                for(int j = 0; j < n; j++){
                        printf("%d\t", mat[i][j]);
                }
                printf("\n");
        }

	printf("\n\n");

}
