#include <stdio.h>
#define SIZE 50
int main(){

	char s[SIZE] = {};
	char check;
	int count = 0;
	int len = 0;

	//get input
	printf("Enter string: ");
	fgets(s, SIZE+1, stdin);

	//get character
	printf("Enter counting character: ");
	scanf("%c", &check);

	//set len
	for(int i = 1; i <= SIZE; i++){
		if(s[i-1] == '\0') break;
		else len = i;
	}

	//count
	for(int i = 0; i < len; i++){
		if(s[i] == check) count++;
	}

	//output 
	printf("Number of %c in string: %d\n", check, count);
	return 0;
}
