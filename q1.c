#include<stdio.h>
#include<string.h>
#define SIZE 100
int main()
{
	char s[SIZE] = {};
	char backup;
	int len = 0;

	//get input
	printf("Please input sentence. ");
	fgets(s, SIZE+1, stdin);

	//calculate string length with spaces
	for(int i = 1; i <= SIZE; i++)
	{
		if(s[i-1] == '\0') break;
		else 
		{
			len = i;
		}
	}
	puts(s);
	
	//reverse operation
	for(int i = 0; i < len / 2; i++)
	{
		backup = s[i];
		s[i] = s[len - (i + 1)];
		s[len - (i + 1)] = backup;
	}

	printf("After reverse.\n");
	puts(s);
	return 0;
}
